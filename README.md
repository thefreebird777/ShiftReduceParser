Creating a Shift-Reduce Parser for the following grammar:

E -> E + T | E - T | T

T -> T * F | T / F | F

F -> ( E ) | n