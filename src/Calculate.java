import java.util.Stack;

/**
 * Created by Ian on 10/15/2016.
 */
public class Calculate {

    StringBuilder sb;
    String token = "";
    int length = 0;
    String exp = "";
    Stack<String> opStack = new Stack<>();
    Stack<Integer> vStack = new Stack<>();
    int ans = 0;
    int num;

    /**
     * Constructor
     */
    public Calculate(String exp, int length) {
        this.exp = exp;
        this.length = length;
    }

    /**
     * gets a token from the input string
     *
     * @return token
     */
    public String getToken() {
        token = "";
        for (int i = 0; i < length; i++) {
            if (sb.charAt(0) != '+' && sb.charAt(0) != '-' &&
                    sb.charAt(0) != '*' && sb.charAt(0) != '/'
                    && sb.charAt(0) != '(' && sb.charAt(0) != ')' && sb.charAt(0) != '$') {
                token += sb.charAt(0);
                sb.deleteCharAt(0);
            } else {
                if (token.isEmpty()) {
                    token += sb.charAt(0);
                    sb.deleteCharAt(0);
                }
                break;
            }
        }
        return token;
    }

    /**
     * evaulates mathematical integer expression
     * @return
     * @throws Exception
     */
    public int eval() throws Exception {
        sb = new StringBuilder(exp);
        while (!sb.toString().isEmpty()) {
            getToken();
            if (token.equals("$")) {
                break;
            }
            if (!token.equals("+") && !token.equals("-") && !token.equals("*") && !token.equals("/") && !token.equals("(") && !token.equals(")")) {
                num = Integer.parseInt(token);
                vStack.push(num);
            } else if (token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/") || token.equals("(") || token.equals(")")) {
                if (opStack.size() == 0) {
                    opStack.push(token);
                } else if (precedence(token) > precedence(opStack.peek())) {
                    opStack.push(token);
                } else if (precedence(token) == precedence(opStack.peek()) && (!token.equals("(")) && (!token.equals(")"))) {
                    String temp = token;
                    token = opStack.pop();
                    opStack.push(temp);
                    int value1 = vStack.pop();
                    int value2 = vStack.pop();
                    switch (token.charAt(0)) {
                        case '*':
                            ans = value1 * value2;
                            vStack.push(ans);
                            break;
                        case '/':
                            if (value1 == 0) {
                                throw new Exception("Cannot divide by 0.");
                            } else {
                                ans = value2 / value1;
                                vStack.push(ans);
                            }
                            break;
                        case '+':
                            ans = value1 + value2;
                            vStack.push(ans);
                            break;
                        case '-':
                            ans = value2 - value1;
                            vStack.push(ans);
                            break;
                    }
                } else if (precedence(token) < precedence(opStack.peek()) && (!token.equals("(")) && (!token.equals(")"))) {
                    String temp = token;
                    token = opStack.pop();
                    opStack.push(temp);
                    int value1 = vStack.pop();
                    int value2 = vStack.pop();
                    switch (token.charAt(0)) {
                        case '*':
                            ans = value1 * value2;
                            vStack.push(ans);
                            break;
                        case '/':
                            if (value1 == 0) {
                                throw new Exception("Cannot divide by 0.");
                            } else {
                                ans = value2 / value1;
                                vStack.push(ans);
                            }
                            break;
                        case '+':
                            ans = value1 + value2;
                            vStack.push(ans);
                            break;
                        case '-':
                            ans = value2 - value1;
                            vStack.push(ans);
                            break;
                    }
                } else if (token.equals("(")) {
                    opStack.push(token);
                } else if (token.equals(")")) {
                    while (!opStack.peek().equals("(")) {
                        token = opStack.pop();
                        int value1 = vStack.pop();
                        int value2 = vStack.pop();
                        switch (token.charAt(0)) {
                            case '*':
                                ans = value1 * value2;
                                vStack.push(ans);
                                break;
                            case '/':
                                if (value1 == 0) {
                                    throw new Exception("Cannot divide by 0.");
                                } else {
                                    ans = value2 / value1;
                                    vStack.push(ans);
                                }
                                break;
                            case '+':
                                ans = value1 + value2;
                                vStack.push(ans);
                                break;
                            case '-':
                                ans = value2 - value1;
                                vStack.push(ans);
                                break;
                        }
                    }
                    opStack.pop();
                }
            }
        }
        while (vStack.size() != 1) {
            if (vStack.size() > 1) {
                int value1 = vStack.pop();
                int value2 = vStack.pop();
                token = opStack.pop();
                switch (token.charAt(0)) {
                    case '*':
                        ans = value1 * value2;
                        vStack.push(ans);
                        break;
                    case '/':
                        if (value1 == 0) {
                            throw new Exception("Cannot divide by 0.");
                        } else {
                            ans = value2 / value1;
                            vStack.push(ans);
                        }
                        break;
                    case '+':
                        ans = value1 + value2;
                        vStack.push(ans);
                        break;
                    case '-':
                        ans = value2 - value1;
                        vStack.push(ans);
                        break;
                }
            }
        }
        if (opStack.size() != 0) {
            for (int j = 0; j < opStack.size() - 1; j++) {
                opStack.pop();
            }
        }
        return ans;
    }

    /**
     * determines operator precedence
     * @param token
     * @return
     */
    public static int precedence(String token) {
        int num = -1;
        if (token.equals("-") || token.equals("+")) {
            num = 0;
        } else if (token.equals("*") || token.equals("/")) {
            num = 1;
        }
        return num;
    }
}