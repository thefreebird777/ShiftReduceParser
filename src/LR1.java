import java.util.Stack;

/**
 * Created by Ian on 10/14/2016.
 */
public class LR1 {

    StringBuilder sb;
    Stack<String> stack = new Stack<String>();
    String expression = " ";
    String token = "";
    int length = 0;
    int state = 0;
    boolean accept = false;
    Calculate cal;

    /**
     * Constructor
     */
    public LR1(String input) {
        this.expression = input;
        length = expression.length();
        sb = new StringBuilder(expression);
        char q;
        q = sb.charAt(length - 1);
        if (q != '$') {
            expression += "$";
            sb = new StringBuilder(expression);
        }
        cal = new Calculate(expression, length);
    }

    /**
     * gets a token from the input string
     *
     * @return token
     */
    public String getToken() {
        token = "";
        for (int i = 0; i < length; i++) {
            if (sb.charAt(0) != '+' && sb.charAt(0) != '-' &&
                    sb.charAt(0) != '*' && sb.charAt(0) != '/'
                    && sb.charAt(0) != '(' && sb.charAt(0) != ')' && sb.charAt(0) != '$') {
                token += sb.charAt(0);
                sb.deleteCharAt(0);
            } else {
                if (token.isEmpty()) {
                    token += sb.charAt(0);
                    sb.deleteCharAt(0);
                }
                break;
            }
        }
        return token;
    }

    //state methods

    public void state0() {
        if (isInt(token) == true) {
            push(token);
            push("5");
            getToken();
        } else {
            switch (token) {
                case "(":
                    push(token);
                    push("4");
                    getToken();
                    break;
                default:
                    error();
            }
        }

    }

    public void state1() throws Exception {
        switch (token) {
            case "+":
                push(token);
                push("6");
                getToken();
                break;
            case "-":
                push(token);
                push("6");
                getToken();
                break;
            case "$":
                success();
                break;
            default:
                error();
        }
    }

    public void state2() {
        switch (token) {
            case "+":
                pop();
                if (!pop().equals("T")) {
                    error();
                }
                E();
                break;
            case "-":
                pop();
                if (!pop().equals("T")) {
                    error();
                }
                E();
                break;
            case "*":
                push(token);
                push("7");
                getToken();
                break;
            case "/":
                push(token);
                push("7");
                getToken();
                break;
            case ")":
                pop();
                if (!pop().equals("T")) {
                    error();
                }
                E();
                break;
            case "$":
                pop();
                if (!pop().equals("T")) {
                    error();
                }
                E();
                break;
            default:
                error();
        }

    }

    public void state3() {
        switch (token) {
            case "+":
                pop();
                if (!pop().equals("F")) {
                    error();
                }
                T();
                break;
            case "-":
                pop();
                if (!pop().equals("F")) {
                    error();
                }
                T();
                break;
            case "*":
                pop();
                if (!pop().equals("F")) {
                    error();
                }
                T();
                break;
            case "/":
                pop();
                if (!pop().equals("F")) {
                    error();
                }
                T();
                break;
            case ")":
                pop();
                if (!pop().equals("F")) {
                    error();
                }
                T();
                break;
            case "$":
                pop();
                if (!pop().equals("F")) {
                    error();
                }
                T();
                break;
            default:
                error();
        }
    }

    public void state4() {
        if (isInt(token) == true) {
            push(token);
            push("5");
            getToken();
        } else {
            switch (token) {
                case "(":
                    push(token);
                    push("4");
                    getToken();
                    break;
                default:
                    error();
            }
        }
    }

    public void state5() {
        switch (token) {
            case "+":
                pop();
                if (!isInt(pop())) {
                    error();
                }
                F();
                break;
            case "-":
                pop();
                if (!isInt(pop())) {
                    error();
                }
                F();
                break;
            case "*":
                pop();
                if (!isInt(pop())) {
                    error();
                }
                F();
                break;
            case "/":
                pop();
                if (!isInt(pop())) {
                    error();
                }
                F();
                break;
            case ")":
                pop();
                if (!isInt(pop())) {
                    error();
                }
                F();
                break;
            case "$":
                pop();
                if (!isInt(pop())) {
                    error();
                }
                F();
                break;
            default:
                error();
        }
    }

    public void state6() {
        if (isInt(token) == true) {
            push(token);
            push("5");
            getToken();
        } else {
            switch (token) {
                case "(":
                    push(token);
                    push("4");
                    getToken();
                    break;
                default:
                    error();
            }
        }

    }

    public void state7() {
        if (isInt(token) == true) {
            push(token);
            push("5");
            getToken();
        } else {
            switch (token) {
                case "(":
                    push(token);
                    push("4");
                    getToken();
                    break;
                default:
                    error();
            }
        }

    }

    public void state8() {
        switch (token) {
            case "+":
                push(token);
                push("6");
                getToken();
                break;
            case "-":
                push(token);
                push("6");
                getToken();
                break;
            case ")":
                push(token);
                push("11");
                getToken();
                break;
            default:
                error();

        }
    }

    public void state9() {
        if (token.equals("+") || token.equals("-") || token.equals(")") || token.equals("$")) {
            pop();
            if (!pop().equals("T")) {
                error();
            }
            pop();
            if (stack.peek().equals("+")) {
                pop();
            }else if(stack.peek().equals("-")){
                pop();
            }else {
                error();
            }
            pop();
            if (!pop().equals("E")) {
                error();
            }
            T();

        } else if (token.equals("*")) {
            push(token);
            push("7");
            getToken();
        } else if (token.equals("/")) {
            push(token);
            push("7");
            getToken();
        } else {
            error();
        }

    }

    public void state10() {
        if (token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/") || token.equals(")") || token.equals("$")) {
            pop();
            if (!pop().equals("F")) {
                error();
            }
            pop();
            if (stack.peek().equals("*")) {
                pop();
            }else if(stack.peek().equals("/")){
                pop();
            }else {
                error();
            }
            pop();
            if (!pop().equals("T")) {
                error();
            }
            T();
        } else {
            error();
        }
    }

    public void state11() {
        if (token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/") || token.equals(")") || token.equals("$")) {
            pop();
            if (!pop().equals(")")) {
                error();
            }
            pop();
            if (!pop().equals("E")) {
                error();
            }
            pop();
            if (!pop().equals("(")) {
                error();
            }
            F();
        } else {
            error();
        }
    }

    //non-terminal symbol methods

    public void F() {
        if (stack.peek().equals("0")) {
            push("F");
            push("3");
        } else if (stack.peek().equals("4")) {
            push("F");
            push("3");
        } else if (stack.peek().equals("6")) {
            push("F");
            push("3");
        } else if (stack.peek().equals("7")) {
            push("F");
            push("10");
        } else {
            error();
        }
    }

    public void T() {
        if (stack.peek().equals("0")) {
            push("T");
            push("2");
        } else if (stack.peek().equals("4")) {
            push("T");
            push("2");
        } else if (stack.peek().equals("6")) {
            push("T");
            push("9");
        } else {
            error();
        }
    }

    public void E() {
        if (stack.peek().equals("0")) {
            push("E");
            push("1");
        } else if (stack.peek().equals("4")) {
            push("E");
            push("8");
        } else {
            error();
        }
    }

    /**
     * directs parser to the next state
     * @throws Exception
     */
    public void action() throws Exception {
        if (isInt(stack.peek())) {
            state = Integer.parseInt(stack.peek());
        }
        switch (state) {
            case 0:
                state0();
                break;
            case 1:
                state1();
                break;
            case 2:
                state2();
                break;
            case 3:
                state3();
                break;
            case 4:
                state4();
                break;
            case 5:
                state5();
                break;
            case 6:
                state6();
                break;
            case 7:
                state7();
                break;
            case 8:
                state8();
                break;
            case 9:
                state9();
                break;
            case 10:
                state10();
                break;
            case 11:
                state11();
                break;
            default:
                error();
        }
    }

    /**
     * checks if input is an integer
     *
     * @param str
     * @return true if integer false if not
     */
    public static boolean isInt(String str) {
        try {
            int i = Integer.parseInt(str);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    /**
     * stops program if the user inputs an invalid expression
     */
    public void error() {
        System.out.println("Expression is invalid");
        System.exit(0);
    }

    /**
     * stops program if it is a success
     */
    public void success() throws Exception {
        System.out.println("Valid Expression = " + cal.eval());
        accept = true;
    }

    /**
     * pushes item to stack and prints stack
     * @param str
     */
    public void push(String str) {
        stack.push(str);
        System.out.println(stack.toString() + "     " + token + sb);
    }

    /**
     * pops item from stack and prints stack
     * @return
     */
    public String pop() {
        String sym = stack.pop();
        System.out.println(stack.toString() + "     " + token + sb);
        return sym;
    }

    /**
     * starts algorithm
     */
    public void run() throws Exception {
        stack.push("0");
        getToken();
        while (!accept) {
            action();
        }
    }

    /**
     * main method, accepts user argument
     *
     * @param args
     */
    public static void main(String[] args) throws Exception {
        LR1 parser = new LR1(args[0]);
        parser.run();
    }
}

